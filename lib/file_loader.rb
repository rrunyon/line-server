module FileLoader
  NEWLINE_INDEX = {}

  def self.initialize_newline_index
    return if filename.blank?

    # the offset of the first line is 0
    NEWLINE_INDEX[1] = 0

    line_number = 1
    offset = 0

    IO.foreach(filename) do |line|
      offset = NEWLINE_INDEX[line_number + 1] = offset + line.length
      line_number += 1
    end

    # trim the offset for the last character in the file from the index
    NEWLINE_INDEX.delete(line_number)
  end

  def self.get_line(line_number)
    offset = NEWLINE_INDEX[line_number]
    # an out of bounds line number has been requested
    return if offset.blank?

    next_offset = NEWLINE_INDEX[line_number + 1]
    # if this is the last line in the file there is no offset, read to the end of the file
    length = next_offset.blank? ? nil : next_offset - offset
    IO.binread(filename, length, offset)
  end

  def self.filename
    ENV['FILENAME']
  end
end
