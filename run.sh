#!/bin/bash

if [ -z "$1" ]; then
  echo "Please provide filename to run server. Example: ./run.sh foobar.txt"
  exit 1
elif [ ! -e $1 ]; then
  echo "$1 not found in current directory, please double check."
  exit 1
else
  FILENAME=$1 rails s
fi
