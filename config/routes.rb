Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :lines, only: [:show] do
    collection do
      get :debug
    end
  end
end
