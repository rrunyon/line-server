class LinesController < ApplicationController

  def show
    line_number = params[:id].to_i
    line = FileLoader.get_line(line_number)

    return head 413 if line.blank?

    render plain: line
  end
end
