# Salsify line server (https://salsify.github.io/line-server.html)


## How does your system work? (if not addressed in comments in source)
This system is a webserver application built with Ruby on Rails. Via the provided bash script, it accepts a single command line argument which points to a file in the project's directory. When the application is booting but before the webserver boots, the text file is read line by line, and a hashtable of `line number -> leading newline offset` is built in memory. Once this operation completes, the webserver boots and a REST API composed of a single endpoint is exposed. When a request is made, the line number requested in the URL is used to look up the leading and trailing newline offsets for the requested line, and read out of the file using `IO.binread`. 

## How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?
The application's performance once booted is acceptable since a web request only results in a hash lookup and a read operation on the file at exactly the location of the requested line. This operation's performance should not vary significantly with the size of the file (assuming the index itself does not exceed memory limits). However, the application is severely hampered by slow boot times at increasingly larger file sizes. I assumed the boot time would scale linearly with the size of the file being provided, however I discovered that this was not true when jumping from 1GB to 10GB. At this file size the Rails process consumes far more memory than at smaller sizes and the system begins to swap aggressively. There may be something in the implementation of `IO.foreach` that I was not anticipating, and as a result the system is not optimized to work with files larger than a few GB in size. Another consideration is not only the size of the file, but the number of lines in the file too. The design that I have chosen consumes memory in correlation with the number of lines in the file, and not necessarily the size of the file. If the file had exceptionally large lines, then the bottleneck would move beyond the boot phase of the app as reading giant strings into memory would slow down the response lifecycle of the webserver.

## How will your system perform with 100 users? 10000 users? 1000000 users?
As a high level test, hitting the server locally with 1000 sequential cURL requests the app manages to respond in under 1ms in almost every case. This does not simulate the kind of concurrency that would be experienced on the web, and single threaded Rails apps are not known for their ability to handle concurrent user requests, so in order to support greater and greater volumes of web traffic I would recommend running this service on multiple server instances with several webserver threads each. I expect if this application were exposed to 1,000,000 users issuing simultaneous requests, running on the hardware on my laptop, the resources would lock and take the server down.

## What documentation, websites, papers, etc did you consult in doing this assignment?
Besides the usual googling/Stack Overflow I made heavy use of Ruby's documentation for the [IO class](http://ruby-doc.org/core-2.3.1/IO.html) and [this](https://stackoverflow.com/questions/25189262/why-is-slurping-a-file-not-a-good-practice) specific Stack Overflow post.

## What third-party libraries or other tools does the system use? How did you choose each library or framework you used?
I chose to build this service with Rails since I have experience with it. Knowing how to easily configure the API and write tests allowed me to do that quickly and save time for solving the actual problem of the exercise.

I used two additional libraries to help with this exercise. The first is Bumbler, a Ruby gem that can be used to profile various parts of your app. I used it specifically to time my initializers since that is where the performance degraded significantly with the size of the file input. I also used RSpec since I'm far more familiar with RSpec than Minitest and wanted to provide a baseline of testing in order to iterate on this service with confidence.

## How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?
I spent four to five hours on this exercise, with a rough even split between researching and implementing. Not being familiar with large-scale disk IO I implemented a rough proof of concept with a naive solution: read the entire file into memory, split on newlines, and index into the array on web requests in order to get the requested line. This fell over very quickly as the size of the file increased and I exceeded the memory space my OS allocated to the Rails process. [This](https://stackoverflow.com/questions/25189262/why-is-slurping-a-file-not-a-good-practice) Stack Overflow post led me to my second implementation by informing me of my options for saving memory.

If I were to revisit this application, I would do two things in the following order:

1.  Address the long boot time for large files. I imagine there is a lot of start up performance that could be gained by a solution that chunks the file, and parallel processes separate indices for those chunks. I could then add more threads or use asynchronous job processing to get the server booted faster.
2.  Write additional tests. I'm a big fan of testing as a way to both frame the problem and provide a safety net against regressions. Of course, when building production software we are often constrained by either time or budget and testing is sometimes given second priority over the deliverable. This exercise was an example of that economic problem.

## If you were to critique your code, what would you have to say about it?
Rails is not the ideal technology choice for what I have concluded is a service that would benefit greatly from native multi-threading and a more performant runtime.

## Additional docs
*  `rspec` to run tests