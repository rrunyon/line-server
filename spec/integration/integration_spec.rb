require 'rails_helper'

describe 'line server' do

  let!(:lines) {
    [
      "line1\n",
      "line2\n",
      "line3\n"
    ]
  }

  # put a timestamp in the filename to avoid a name collision with user created files
  let!(:filename) { "temp#{Time.now}.txt" }

  before do
    # create a test file and write our lines to it
    file = File.new(filename, 'w')
    lines.each { |line| file.write(line) }
    file.close

    # set the filename on the environment
    ENV['FILENAME'] = filename

    # call the file loading module to prepare the index
    FileLoader.initialize_newline_index
  end

  describe 'FileLoader::NEWLINE_INDEX' do

    it 'prepares an index with a length equal to the number of lines in the file' do
      expect(FileLoader::NEWLINE_INDEX.length).to eq(3)
    end
  end

  describe 'REST API' do

    it 'returns the line and a 200 response code when in bounds' do
      lines.each_with_index do |line, i|
        get "/lines/#{i+1}"
        expect(response.status).to eq(200)
        expect(response.body).to eq(line)
      end
    end

    it 'returns a 413 when out of bounds' do
      line_number = lines.length + 1

      get "/lines/#{line_number}"
      expect(response.status).to eq(413)
      expect(response.body.blank?).to eq(true)
    end
  end

  after do
    File.delete(filename)
  end
end